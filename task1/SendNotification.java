package task1;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SendNotification {
	
	WebDriver driver;
	
	@DataProvider(name="Notifications")
	public String[][] notificationData() {
		String[][] notifications = getExcelData();
		return notifications;
	}
	
	@BeforeTest
	public void login(){
		
		Reporter.log("Initializing Firefox driver", true);
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		
		Reporter.log("Open admin console page", true);
		driver.get("https://pushcrew.com/admin");
		
		Reporter.log("Wait for Page to load", true);
		waitForPageToLoad(driver);
		
		Reporter.log("Enter Username and password for login", true);
		driver.findElement(By.xpath("//input[@id='email']")).sendKeys("hemantbanafal@gmail.com");;
		driver.findElement(By.id("password")).sendKeys("password");
		driver.findElement(By.xpath("//button//*[text()='Sign in']")).submit();
		
		Reporter.log("Wait for Admin console to load", true);
		waitForPageToLoad(driver);
		
		Reporter.log("Wait for Send Notification Button to be clickable", true);
		waitForElement(driver, "//nav//div//a[@href='send-notification.php']", "clickable");
		
	}
		
	@Test(dataProvider="Notifications")
	public void sendNotifications(String notificationTitle, String notificationMessage){
		
		Reporter.log("Clicking on Send Notification button", true);
		driver.findElement(By.xpath("//nav//div//a[@href='send-notification.php']")).click();
		waitForPageToLoad(driver);
		
		Reporter.log("Enter the Notification Title, Message and Offer URL Page", true);
		driver.findElement(By.id("title")).sendKeys(notificationTitle);
		driver.findElement(By.xpath("//textarea[@id='message' and @title='100 Characters max']")).sendKeys(notificationMessage);
		driver.findElement(By.id("notification-property_offerpageURL")).sendKeys("http://hemantsingh.s3-website.ap-south-1.amazonaws.com/");
		
		Reporter.log("Sending the Notification", true);
		driver.findElement(By.xpath("//form[@id='new-notification']//button[text()='Send Notification']")).click();
		
		Reporter.log("Waiting for the Confirmation Pop up to appear and clicking on the final comnfirmation to send notification", true);
		waitForElement(driver, "//div[@id='confirmModal']//button[contains(text(),'Send Notification')]", "visible");
		driver.findElement(By.xpath("//div[@id='confirmModal']//button[contains(text(),'Send Notification')]")).click();
		
		Reporter.log("Waiting for success message pop up to appear", true);
		waitForElement(driver, "//div[@id='confirmModal']//div[@class='modal-body row']//div[@id='js-modal-body2']//p", "visible");
	  
		Reporter.log("Getting the Success Mesaage", true);
		String successMessage = driver.findElement(By.xpath("//div[@id='confirmModal']//div[@class='modal-body row']//div[@id='js-modal-body2']//p")).getText();
		Reporter.log("Actual Success Message is : " + successMessage);
		
		Reporter.log("Comparing Actual Message with the Expected Message", true);
	    Assert.assertTrue(successMessage.equals("Your notification has been sent successfully."));
	    
	    Reporter.log("Closing Success Message pop up", true);
	    driver.findElement(By.xpath("//div[@id='confirmModal']//button[text()='Close']")).click();
	}
	
	
	// This method is to ensure the page is loaded successfully
	public static void waitForPageToLoad(WebDriver driver) {
		
		 WebDriverWait wait = new WebDriverWait(driver, 30); 
		 wait.until(ExpectedConditions.jsReturnsValue("return document.readyState==\"complete\";"));

	}
	
	// This method is to check for an element if that is visible or clickable
	public static void waitForElement(WebDriver driver, String xpathLocator, String valitdationType){
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		
		if(valitdationType.equals("clickable"))
		 wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpathLocator)));
		 
		else if(valitdationType.equals("visible"))
		 wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathLocator)));

	}
	
	/* This method is to fetch data from the Excel sheet assuming that Sheet has only 10 rows and 2 
	columns containing Title and Message of the Notification */
	
	public String[][] getExcelData() {
		 String excelFilePath = "D:\\Notifications.xlsx";
	        FileInputStream inputStream = null;
	        Workbook workbook = null;
			try {
				inputStream = new FileInputStream(new File(excelFilePath));
				workbook = new XSSFWorkbook(inputStream);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	         
	        Sheet firstSheet = workbook.getSheetAt(0);
	       
	        String[][] arrayExcelData = new String[10][2];
			
			for (int i= 0 ; i < 10; i++) {

				Row row = firstSheet.getRow(i);
				for (int j=0; j < 2; j++) {
					arrayExcelData[i][j] = row.getCell(j).getStringCellValue();
				}

			}
	                  
	        try {
				workbook.close();
				inputStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			return arrayExcelData;
	    }
	 

	
}
