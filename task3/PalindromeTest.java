package task3;

import java.util.Scanner;

public class PalindromeTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Reading from System.in");
		Scanner reader = new Scanner(System.in); 
		System.out.println("Enter a number: ");
		int number = reader.nextInt(); 

		int remainder, sum = 0, temp = number;    

        while(number > 0){    
        	remainder = number%10;  
		    sum = (sum*10) + remainder;    
		    number = number/10;    
		  }    
		  if(temp==sum)    
		   System.out.println("This is a Palindrome number ");    
		  else    
		   System.out.println("This is not a Palindrome number");    
	}

}
