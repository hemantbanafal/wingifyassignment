package task3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileReader {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		try {
			System.out.println("Provide File path to scanner");
			Scanner inputFile = new Scanner(new File("D:\\FileReader.txt"));
		
			System.out.println("Reading the file and print line by line on console");
		    String line;
		    while(inputFile.hasNextLine())
		   {
		    line = inputFile.nextLine();
		    System.out.println(line);
		   }
		
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
